.PHONY: up down

network := traefik

up:
	docker network create $(network) || true
	docker-compose up -d

down:
	docker-compose down
	docker network rm $(network) || true
