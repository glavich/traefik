# Traefik

## Prerequisite

- git (https://git-scm.com/)
- docker (https://docs.docker.com/docker-for-windows/install/)
- docker-compose (https://docs.docker.com/compose/install/)
- make (http://gnuwin32.sourceforge.net/packages/make.htm)

If you are using Chocolatey (https://chocolatey.org/) just run: `choco install git docker-desktop docker-compose make`

## Git clone

To clone this repository run: `git clone https://gitlab.com/glavich/traefik.git` or use ssh, whatever you prefer.

## Run

Run: `make`
